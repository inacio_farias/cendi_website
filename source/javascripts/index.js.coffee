$ ->
  ###################################
  ##                               ##
  ## Handling clicks to open modal ##
  ##                               ##
  ###################################
  $(document).on 'click', '.modal-box', (evt)->
    # evt.stopPropagation()
    # evt.preventDefault()

  $(document).on 'click', '.modal-overlay', (evt)->
    overlay = $(@)
    overlay.closest('.modal').find('.modal-box').toggleClass('active')

  $(document).on 'click', '[modal-trigger]', (evt)->
    modalId = $(@).data('trigger-for')
    modal   = $("##{modalId}")
    modal.find('.modal-box').addClass('active')
    $(modal.find('img')[0]).addClass('active')
    resizeModalHeight(modal)


  ###################################################
  ##                                               ##
  ## Handling clicks to slide through modal images ##
  ##                                               ##
  ###################################################
  $(document).on 'click', '.modal-box .left-arrow', (evt)->
    changeImage($(@).closest('.modal'), -1)

  $(document).on 'click', '.modal-box .right-arrow', (evt)->
    changeImage($(@).closest('.modal'), 1)

  changeImage = (modal, modifier)->
    current = modal.find('img.active')
    all     = modal.find('img')
    idx     = all.index(current)
    nextIdx = if (idx + modifier) >= 0
                (idx + modifier) % all.length
              else
                all.length - 1

    current.removeClass('active')
    $(modal.find('img')[nextIdx]).addClass('active')
    resizeModalHeight(modal)

  resizeModalHeight = (modal)->
    image     = modal.find('img.active')
    maxHeight = Math.min.apply(null, [image.height(), $(window).height() * 0.85])
    modal.find('.modal-box').css 'max-height': maxHeight

